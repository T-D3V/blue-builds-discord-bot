from node:latest as build
RUN apt update && apt install -y --no-install-recommends dumb-init
WORKDIR /app
COPY package*.json /app
RUN npm ci --only=production
FROM node:20-slim
COPY --from=build /usr/bin/dumb-init /usr/bin/dumb-init
WORKDIR /app
ENV NODE_ENV production
USER node
COPY --chown=node:node --from=build /app/node_modules /app/node_modules
COPY --chown=node:node . /app
CMD ["dumb-init" ,"node", "index.js"]