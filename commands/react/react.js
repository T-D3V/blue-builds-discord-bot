const {
  Client,
  Events,
  GatewayIntentBits,
  Guilds,
  EmbedBuilder,
  MessageManager,
  Embed,
  Collection,
  GuildMember,
  GuildHubType,
  SlashCommandBuilder,
  ButtonBuilder,
  ActionRowBuilder,
  ButtonStyle,
  StringSelectMenuBuilder,
  StringSelectMenuOptionBuilder,
  PermissionFlagsBits,
} = require('discord.js');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('react')
    .setDescription('Add reactions to messages containing specific links'),
  async execute(interaction) {
    interaction.deferReply({ ephemeral: true });
    const allowedRole = '1103119794275897347';
    const userRoles = interaction.member.roles.cache;

    // Check if it's outside the specified channel
    if (!userRoles.has(allowedRole)) {
      return interaction.editReply({
        content: 'BlueBot commands are to be used in <#962377198315130941>!',
        ephemeral: true,
      });
    }

    const channelMappings = {
      archer: '1016896287007842415',
      warrior: '1016896349301653616',
      shaman: '1016896429584814240',
      mage: '1016896363461611550',
      assassin: '1016896402082758738',
    };

    const batchSize = 100;
    const channelsToSearch = Object.values(channelMappings).map(channelId =>
      interaction.guild.channels.cache.get(channelId),
    );
    const messages = await fetchMessagesFromDiscord(
      channelsToSearch,
      batchSize,
    );

    const linksToSearch = [
      'https://wynnbuilder.github.io/',
      'https://hppeng-wynn.github.io/',
    ];

    const reactionEmoji = '<:Like:1183573343627137087>';

    messages.forEach(async message => {
      const messageContent = message.content || '';

      if (
        linksToSearch.some(link => messageContent.includes(link)) &&
        !message.reactions.cache.some(
          reaction => reaction.emoji.name === reactionEmoji,
        )
      ) {
        try {
          await message.react(reactionEmoji);
        } catch (error) {
          console.error(
            `Error adding reaction to message ${message.id}: ${error.message}`,
          );
        }
      }
    });

    interaction.editReply({
      content: 'Reactions added to messages containing specific links.',
    });
  },
};

async function fetchMessagesFromDiscord(channels, batchSize) {
  let allMessages = [];

  for (const channel of channels) {
    let lastMessageId = null;

    while (true) {
      const options = { limit: batchSize, before: lastMessageId };
      const messages = await channel.messages.fetch(options);

      if (messages.size === 0) {
        // No more messages
        break;
      }

      lastMessageId = messages.last().id;
      allMessages.push(...messages.values());

      // Ensure we don't exceed Discord's rate limits
      await new Promise(resolve => setTimeout(resolve, 250));
    }
  }

  return allMessages;
}
